package com.piserve.geejo.msk;


import android.app.LoaderManager;
import android.app.Service;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.piserve.geejo.msk.data.ServiceAdapter;
import com.piserve.geejo.msk.data.ServiceContract;

public class MSKFragment extends android.support.v4.app.Fragment implements android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor>
{
    private ServiceAdapter mServiceAdapter;

    public static final int SERVICE_LOADER = 0;

    private static final String[] SERVICE_COLUMNS =
            {
                    ServiceContract.ServiceEntry.TABLE_NAME + "." + ServiceContract.ServiceEntry._ID,
                    ServiceContract.ServiceEntry.COLUMN_NAME,
                    ServiceContract.ServiceEntry.COLUMN_ORGANIZATION_NAME,
                    ServiceContract.ServiceEntry.COLUMN_PRICE
            };

    public static final int COL_ID = 0;
    public static final int COL_NAME = 1;
    public static final int COL_ORG_NAME = 2;
    public static final int COL_PRICE = 3;

    public MSKFragment()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.menu_fragment_msk, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_refresh)
        {
            //Moving functions to a helper class, so that when Activity starts the data can be displayed
            updateServices();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        mServiceAdapter = new ServiceAdapter(getActivity(), null, 0);

        View rootView = inflater.inflate(R.layout.fragment_msk, container, false);

        ListView listView = (ListView) rootView.findViewById(R.id.listViewMainService);

        listView.setAdapter(mServiceAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Cursor cursor = (Cursor) parent.getItemAtPosition(position);
                if(cursor!=null)
                {
                    Intent detailIntent = new Intent(getActivity(), MSKDetail.class).setData(ServiceContract.ServiceEntry.buildServiceUri(id));
                    startActivity(detailIntent);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        getLoaderManager().initLoader(SERVICE_LOADER, null, this).forceLoad();
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        //Refresh called and service updated when activity starts
        updateServices();
    }

    //Calling FetchServiceTask and executing it.
    private void updateServices()
    {
        FetchServicesTask serviceTask = new FetchServicesTask(getActivity());
        serviceTask.execute();
    }

    @Override
    public android.support.v4.content.Loader<Cursor> onCreateLoader(int i, Bundle args)
    {
        Uri serviceUri = ServiceContract.ServiceEntry.buildServiceUri(i);
        return new android.support.v4.content.CursorLoader(getActivity(), serviceUri, SERVICE_COLUMNS, null, null, null);
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader, Cursor data)
    {
        mServiceAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader)
    {
        mServiceAdapter.swapCursor(null);
    }

}
