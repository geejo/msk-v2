package com.piserve.geejo.msk.data;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;


import com.piserve.geejo.msk.MSKFragment;
import com.piserve.geejo.msk.R;

public class ServiceAdapter extends CursorAdapter
{
    public ServiceAdapter(Context context, Cursor c, int flags)
    {
        super(context, c, flags);
    }

    private String convertCursorRowToUXFormat(Cursor cursor)
    {

        return cursor.getString(MSKFragment.COL_NAME) + "-" + cursor.getString(MSKFragment.COL_ORG_NAME) +
                " - " + cursor.getInt(MSKFragment.COL_PRICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_main, parent, false);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
        TextView tv = (TextView)view;
        tv.setText(convertCursorRowToUXFormat(cursor));
    }
}
