package com.piserve.geejo.msk;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.piserve.geejo.msk.data.ServiceContract;
import com.piserve.geejo.msk.data.ServiceContract.ServiceEntry;


public  class MSKDetail extends ActionBarActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_detail);
        if (savedInstanceState == null)
        {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new DetailFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_mskdetail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_settings)
        {
            startActivity(new Intent(this, MSKSettings.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class DetailFragment extends android.support.v4.app.Fragment implements LoaderManager.LoaderCallbacks<Cursor>
    {
        public static final String LOG_TAG = MSKDetail.class.getSimpleName();
        private String mServiceStr;
        private static final int DETAIL_LOADER = 0;

        private static final String[] SERVICE_COLUMNS =
                {
                        ServiceEntry.TABLE_NAME + "." + ServiceEntry._ID,
                        ServiceEntry.COLUMN_NAME,
                        ServiceEntry.COLUMN_ORGANIZATION_NAME,
                        ServiceEntry.COLUMN_PRICE
                };

        public static final int COL_ID = 0;
        public static final int COL_NAME = 1;
        public static final int COL_ORG_NAME = 2;
        public static final int COL_PRICE = 3;

        public DetailFragment()
        {
            setHasOptionsMenu(true);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState)
        {
            View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
            TextView detailTextView = (TextView) rootView.findViewById(R.id.detailTextView);

            Intent detailIntent = getActivity().getIntent();

            if(detailIntent!=null && detailIntent.hasExtra(Intent.EXTRA_TEXT))
            {
                mServiceStr = detailIntent.getStringExtra(Intent.EXTRA_TEXT);
                detailTextView.setText(mServiceStr);
            }

            return rootView;
        }

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args)
        {
            Log.v(LOG_TAG, "In onCreateLoader");
            Intent detailIntent = getActivity().getIntent();
            if(detailIntent==null){return null;}

            return new CursorLoader(getActivity(), detailIntent.getData(), SERVICE_COLUMNS, null, null, null);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data)
        {
            Log.v(LOG_TAG, "In onLoadFinished");
            if(!data.moveToFirst()) { return; }

            String name = data.getString(COL_NAME);
            String organizationName = data.getString(COL_ORG_NAME);
            int price = data.getInt(COL_PRICE);

            mServiceStr = String.format("%s - %s - % i", name, organizationName, price);

            TextView detailTextView = (TextView) getView().findViewById(R.id.detailTextView);
            detailTextView.setText(mServiceStr);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader)
        {

        }
    }

}
