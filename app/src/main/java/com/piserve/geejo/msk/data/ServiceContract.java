package com.piserve.geejo.msk.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class ServiceContract
{
    public static final String CONTENT_AUTHORITY = "com.piserve.geejo.msk";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_SERVICE = "service";

    // Next for each table create Uri's for ServiceEntry

    ////////////////////////////////////////////////////////////////////////////////////////////

    public static final class ServiceEntry implements BaseColumns
    {

        //For SQLite: Represents Base Location for Service table.
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_SERVICE).build();

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SERVICE;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SERVICE;

        public static final String TABLE_NAME = "service";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_ORGANIZATION_NAME = "organization_name";
        public static final String COLUMN_PRICE = "price";
        public static final String COLUMN_STATE = "state";
        public static final String COLUMN_SMALL_PICTURE = "small_picture";

        ////////////////////////////////////////////////////////////////////////////////////////////

        /*Adding URI builder and decoder functions.*/

        public static Uri buildServiceUri(long id)
        {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildServiceWithState(String region)
        {
            return CONTENT_URI.buildUpon().appendPath(region).build();
        }

        public static Uri buildServiceWithCategory(String category)
        {
            return CONTENT_URI.buildUpon().appendPath(category).build();
        }

    }
}
