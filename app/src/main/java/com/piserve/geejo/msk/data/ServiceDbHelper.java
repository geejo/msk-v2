package com.piserve.geejo.msk.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ServiceDbHelper extends SQLiteOpenHelper
{
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "service.db";

    public ServiceDbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        final String SQL_CREATE_SERVICE_TABLE =
                        "CREATE TABLE " +
                        ServiceContract.ServiceEntry.TABLE_NAME +
                        " (" +
                            ServiceContract.ServiceEntry._ID + " INTEGER PRIMARY KEY," +
                            ServiceContract.ServiceEntry.COLUMN_NAME + " TEXT NOT NULL," +
                            ServiceContract.ServiceEntry.COLUMN_ORGANIZATION_NAME + " TEXT NOT NULL," +
                            ServiceContract.ServiceEntry.COLUMN_PRICE + " INTEGER NOT NULL" +
                         ");";

        sqLiteDatabase.execSQL(SQL_CREATE_SERVICE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion)
    {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS" + ServiceContract.ServiceEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
