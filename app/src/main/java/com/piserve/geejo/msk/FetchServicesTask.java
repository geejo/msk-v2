package com.piserve.geejo.msk;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.piserve.geejo.msk.data.ServiceContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

public class FetchServicesTask extends AsyncTask<String, Void, Void>
{
    private final String LOG_TAG = FetchServicesTask.class.getSimpleName();

    private final Context mContext;

    private boolean DEBUG = true;

    public FetchServicesTask(Context context )
    {
        mContext = context;
    }

    private String[] getServicesDatafromJson(String serviceJsonStr) throws JSONException
    {
        final String MSK_NAME = "name";
        final String MSK_PRICE = "price";
        final String MSK_ORG_NAME = "organizationName";
        final String MSK_POSTED_USER = "postedUser";
        final String MSK_PROFILE = "profile";

        int totalResults = 25;
        String resultStrs[] = new String[totalResults];

        try
        {
            JSONArray serviceArray = new JSONArray(serviceJsonStr);

            Vector<ContentValues> cVVector = new Vector<ContentValues>(serviceArray.length());

            for(int i=0;i<serviceArray.length();i++)
            {
                String name, organizationName;
                int price;

                JSONObject serviceObject = serviceArray.getJSONObject(i);

                name =  serviceObject.getString(MSK_NAME);
                price = serviceObject.getInt(MSK_PRICE);

                JSONObject postedUserObject = serviceObject.getJSONObject(MSK_POSTED_USER);
                JSONObject profileObject = postedUserObject.getJSONObject(MSK_PROFILE);

                organizationName = profileObject.getString(MSK_ORG_NAME);

                ContentValues serviceValues = new ContentValues();

                serviceValues.put(ServiceContract.ServiceEntry.COLUMN_NAME, name);
                serviceValues.put(ServiceContract.ServiceEntry.COLUMN_ORGANIZATION_NAME, organizationName);
                serviceValues.put(ServiceContract.ServiceEntry.COLUMN_PRICE, price);

                cVVector.add(serviceValues);
            }

            int inserted = 0;

            if(cVVector.size()>0)
            {
                ContentValues[] cvArray = new ContentValues[cVVector.size()];
                cVVector.toArray(cvArray);

                inserted = mContext.getContentResolver().bulkInsert(ServiceContract.ServiceEntry.CONTENT_URI, cvArray);
            }

            Log.d(LOG_TAG, "FetchServicesTask Complete. " + inserted + " Inserted");
        }

        catch(JSONException e)
        {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
        return null;
    }

    /////////////////////////////// NETWORKING BOILER PLATE ///////////////////////////
    @Override
    protected Void doInBackground(String... params)
    {
        if (params.length == 0)
        {
            return null;
        }

        HttpURLConnection urlConnection = null; //Streaming data using HTTP
        String serviceJsonStr = null; //raw JSON response
        BufferedReader reader = null; //read text from character i/p stream

        int pageNo = 1;
        int numResults = 5;

        try
        {
            //-------------------------CONNECTION--------------------//

            final String SERVICE_BASE_URL = "http://myservicekart.com/public/";
            final String SEARCH_BASE_URL = "http://myservicekart.com/public/search?";
            final String SEARCH_PARAM = "search";
            final String PAGE_PARAM = "pageno";


                /*Using a helper class UriBuilder for building and manipulating URI references*/

            Uri builtServiceUri = Uri.parse(SERVICE_BASE_URL);
            Uri builtSearchUri = Uri.parse(SEARCH_BASE_URL).buildUpon().
                    appendQueryParameter(SEARCH_PARAM, "").
                    appendQueryParameter(PAGE_PARAM, Integer.toString(pageNo)).
                    build();

            URL searchUrl = new URL(builtSearchUri.toString());

            Log.v(LOG_TAG, "Built SearchUri" +builtSearchUri.toString());

            urlConnection = (HttpURLConnection) searchUrl.openConnection();

            urlConnection.setRequestMethod("GET");

            urlConnection.connect();

            //------------------------BUFFERING---------------------//

            InputStream inputStream = urlConnection.getInputStream();

            StringBuffer buffer = new StringBuffer();

            if(inputStream==null)
            {
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line = null;

            while((line = reader.readLine()) != null)
            {
                buffer.append(line + "\n");
            }

            if(buffer.length()==0)
            {
                return null;
            }
            serviceJsonStr = buffer.toString();
            getServicesDatafromJson(serviceJsonStr);
            Log.v(LOG_TAG, "Services JSON String: " +serviceJsonStr);
        }

        catch(IOException e)
        {
            Log.e(LOG_TAG, "Error cannot connect to URL", e);
            return null;
        }

        catch (JSONException e)
        {
            e.printStackTrace();
        }

        finally
        {
            if(urlConnection!=null)
            {
                urlConnection.disconnect();
            }

            if(reader!=null)
            {
                try
                {
                    reader.close();
                }
                catch (final IOException e)
                {
                    Log.e(LOG_TAG,"Error closing stream",e);
                }
            }
        }

        return null;
    }
}
