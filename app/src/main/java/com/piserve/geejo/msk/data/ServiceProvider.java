package com.piserve.geejo.msk.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

/**
 * Created by Droid Space on 4/14/2015.
 */
public class ServiceProvider extends ContentProvider
{
    private com.piserve.geejo.msk.data.ServiceDbHelper mOpenHelper;

    public static final UriMatcher sUriMatcher = buildUriMatcher();

    static final int SERVICE = 100;
    static final int SERVICE_WITH_STATE = 101;
    static final int SERVICE_WITH_CATEGORY = 102;
    static final int CATEGORY = 200;

    static UriMatcher buildUriMatcher()
    {

        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        final String authority = ServiceContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, ServiceContract.PATH_SERVICE, SERVICE);

        return matcher;
    }

    @Override
    public boolean onCreate()
    {
        mOpenHelper = new ServiceDbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        Cursor retCursor;
//        switch (sUriMatcher.match(uri))
//        {
//            case SERVICE:
//            {
                retCursor = mOpenHelper.getReadableDatabase().query
                        (
                                ServiceContract.ServiceEntry.TABLE_NAME,    //table
                                projection,                                 //columns[]
                                selection,
                                selectionArgs,
                                null,                                       //groupBy
                                null,                                       //having
                                sortOrder                                   //orderBy
                        );
 //               break;
 //           }
 //           default:
 //               throw new UnsupportedOperationException("Unknown Uri: " + uri);
 //       }

//        if (!(retCursor.moveToFirst()) || retCursor.getCount() ==0){return null;}
//        int resultSize = retCursor.getCount();
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Override
    public String getType(Uri uri)
    {
        final int match = sUriMatcher.match(uri);

        switch (match)
        {
            case SERVICE:
                return ServiceContract.ServiceEntry.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown Uri" + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match)
        {
            case SERVICE:
            {
                long _id = db.insert(ServiceContract.ServiceEntry.TABLE_NAME, null, values);

                if (_id > 0)
                {
                    returnUri = ServiceContract.ServiceEntry.buildServiceUri(_id);
                }

                else
                {
                    throw new SQLException("Failed to insert row into" + uri);
                }
                break;

            }

            default:
                throw new UnsupportedOperationException("Unknown Uri" + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        db.close();
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        final int match = sUriMatcher.match(uri);
        switch (match)
        {
            case SERVICE:
                db.beginTransaction();
                int returnCount = 0;

                try
                {
                    for (ContentValues value : values)
                    {
                        long _id = db.insert(ServiceContract.ServiceEntry.TABLE_NAME, null, value);
                        if (_id != -1)
                        {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }

                finally
                {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;

            default:
                return super.bulkInsert(uri, values);
        }
    }
}